from bonus_system import calculateBonuses

def testStandard():
    assert round(calculateBonuses('Standard', 0), 2)  == 0.5
    assert round(calculateBonuses('Standard', 10000), 2) == 0.75
    assert round(calculateBonuses('Standard', 50000), 2) == 1
    assert round(calculateBonuses('Standard', 100000), 2) == 1.25
    assert round(calculateBonuses('Standard', 100001), 2) == 1.25

def testPremium():
    assert round(calculateBonuses('Premium', 0), 2) == 0.1
    assert round(calculateBonuses('Premium', 10000), 2) == 0.15
    assert round(calculateBonuses('Premium', 50000), 2) == 0.2
    assert round(calculateBonuses('Premium', 100000), 2) == 0.25
    assert round(calculateBonuses('Premium', 100001), 2) == 0.25
    
def testDiamond():
    assert round(calculateBonuses('Diamond', 0), 2) == 0.2
    assert round(calculateBonuses('Diamond', 10000), 2) == 0.3
    assert round(calculateBonuses('Diamond', 50000), 2) == 0.4
    assert round(calculateBonuses('Diamond', 100000), 2) == 0.5
    assert round(calculateBonuses('Diamond', 100001), 2) == 0.5


def testRandom():
    assert calculateBonuses('A', 10000) == 0
    assert calculateBonuses('Z', 10000) == 0